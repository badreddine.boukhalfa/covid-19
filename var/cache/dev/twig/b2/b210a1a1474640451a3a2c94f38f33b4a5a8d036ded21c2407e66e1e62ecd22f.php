<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* index.html.twig */
class __TwigTemplate_cc745151ea8bd9a21f0bb0e6aaa55eacb79080a55507a3518c168781a4365703 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Information  Patient Covid-19 ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"limiter\">
\t\t<div class=\"container-table100\">
\t\t\t<div class=\"wrap-table100\">
\t\t\t\t<div class=\"table100 ver1 m-b-110\">
\t\t\t\t\t<div class=\"table100-head\">
\t\t\t\t\t\t<table>
\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t<tr class=\"row100 head\">
\t\t\t\t\t\t\t\t\t<th class=\"cell100 column1\">Id</th>
\t\t\t\t\t\t\t\t\t<th class=\"cell100 column2\">Nom </th>
\t\t\t\t\t\t\t\t\t<th class=\"cell100 column3\">Prenom</th>
\t\t\t\t\t\t\t\t\t<th class=\"cell100 column4\">date de naissance</th>
\t\t\t\t\t\t\t\t\t<th class=\"cell100 column5\">adresse</th>
                                    <th class=\"cell100 column6\">téléphone</th>
                                    <th class=\"cell100 column7\">date entrée</th>
                                    <th class=\"cell100 column8\">date sortie</th>
                                    <th class=\"cell100 column9\">Sortie Confi</th>
                                    <th class=\"cell100 column10\">Nombre de jours</th>
                                    <th class=\"cell100 column11\">test covid</th>
\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t</table>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"table100-body js-pscroll\">
\t\t\t\t\t\t<table>
\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t\t";
        // line 33
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["patients"]) || array_key_exists("patients", $context) ? $context["patients"] : (function () { throw new RuntimeError('Variable "patients" does not exist.', 33, $this->source); })()));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["patient"]) {
            // line 34
            echo "                        
                            <tr> 
                                <td class=\"cell100 column1\"><a href=\"";
            // line 36
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("patient_covid_edit", ["id" => twig_get_attribute($this->env, $this->source, $context["patient"], "id", [], "any", false, false, false, 36)]), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["patient"], "id", [], "any", false, false, false, 36), "html", null, true);
            echo " </a></td>
                                <td class=\"cell100 column2\">";
            // line 37
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["patient"], "nom", [], "any", false, false, false, 37), "html", null, true);
            echo "</td>
                                <td class=\"cell100 column3\">";
            // line 38
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["patient"], "Prenom", [], "any", false, false, false, 38), "html", null, true);
            echo "</td> 
                                <td class=\"cell100 column4\">";
            // line 39
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["patient"], "DateDeNaissance", [], "any", false, false, false, 39), "m/d/Y"), "html", null, true);
            echo "</td>
                                <td class=\"cell100 column5\">";
            // line 40
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["patient"], "Adresse", [], "any", false, false, false, 40), "html", null, true);
            echo "</td>
                                <td class=\"cell100 column6\">";
            // line 41
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["patient"], "Telephone", [], "any", false, false, false, 41), "html", null, true);
            echo "</td>
                                <td class=\"cell100 column7\">";
            // line 42
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["patient"], "DateEntrer", [], "any", false, false, false, 42), "m/d/Y"), "html", null, true);
            echo "</td>
                                <td class=\"cell100 column8\">";
            // line 43
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["patient"], "DateSortie", [], "any", false, false, false, 43), "m/d/Y"), "html", null, true);
            echo "</td>
                                <td class=\"cell100 column9\">";
            // line 44
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["patient"], "DateSortieEncasDeProlongation", [], "any", false, false, false, 44), "m/d/Y"), "html", null, true);
            echo "</td>
                                <td class=\"cell100 column10\">";
            // line 45
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["patient"], "NombreDejoursResterDansConfinement", [], "any", false, false, false, 45), "html", null, true);
            echo "</td>
                                <td class=\"cell100 column11\">";
            // line 46
            if (0 === twig_compare(twig_get_attribute($this->env, $this->source, $context["patient"], "TestCovid", [], "any", false, false, false, 46), "TestCovid")) {
                // line 47
                echo "                                    Non rensigne
                                ";
            } else {
                // line 49
                echo "                                      ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["patient"], "TestCovid", [], "any", false, false, false, 49), "html", null, true);
                echo "  
                                ";
            }
            // line 50
            echo "</td>
                            
                            </tr>
                        ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 54
            echo "                            <tr>
                                <td class=\"cell100 column1\">no records found</td>
                            </tr>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['patient'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 58
        echo "
\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t</table>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div> 

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  197 => 58,  188 => 54,  180 => 50,  174 => 49,  170 => 47,  168 => 46,  164 => 45,  160 => 44,  156 => 43,  152 => 42,  148 => 41,  144 => 40,  140 => 39,  136 => 38,  132 => 37,  126 => 36,  122 => 34,  117 => 33,  88 => 6,  78 => 5,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Information  Patient Covid-19 {% endblock %}

{% block body %}
    <div class=\"limiter\">
\t\t<div class=\"container-table100\">
\t\t\t<div class=\"wrap-table100\">
\t\t\t\t<div class=\"table100 ver1 m-b-110\">
\t\t\t\t\t<div class=\"table100-head\">
\t\t\t\t\t\t<table>
\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t<tr class=\"row100 head\">
\t\t\t\t\t\t\t\t\t<th class=\"cell100 column1\">Id</th>
\t\t\t\t\t\t\t\t\t<th class=\"cell100 column2\">Nom </th>
\t\t\t\t\t\t\t\t\t<th class=\"cell100 column3\">Prenom</th>
\t\t\t\t\t\t\t\t\t<th class=\"cell100 column4\">date de naissance</th>
\t\t\t\t\t\t\t\t\t<th class=\"cell100 column5\">adresse</th>
                                    <th class=\"cell100 column6\">téléphone</th>
                                    <th class=\"cell100 column7\">date entrée</th>
                                    <th class=\"cell100 column8\">date sortie</th>
                                    <th class=\"cell100 column9\">Sortie Confi</th>
                                    <th class=\"cell100 column10\">Nombre de jours</th>
                                    <th class=\"cell100 column11\">test covid</th>
\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t</table>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"table100-body js-pscroll\">
\t\t\t\t\t\t<table>
\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t\t{% for patient in patients %}
                        
                            <tr> 
                                <td class=\"cell100 column1\"><a href=\"{{ path('patient_covid_edit', {'id': patient.id}) }}\">{{ patient.id }} </a></td>
                                <td class=\"cell100 column2\">{{ patient.nom }}</td>
                                <td class=\"cell100 column3\">{{ patient.Prenom }}</td> 
                                <td class=\"cell100 column4\">{{ patient.DateDeNaissance|date(\"m/d/Y\")}}</td>
                                <td class=\"cell100 column5\">{{ patient.Adresse }}</td>
                                <td class=\"cell100 column6\">{{ patient.Telephone }}</td>
                                <td class=\"cell100 column7\">{{ patient.DateEntrer|date(\"m/d/Y\") }}</td>
                                <td class=\"cell100 column8\">{{ patient.DateSortie|date(\"m/d/Y\") }}</td>
                                <td class=\"cell100 column9\">{{ patient.DateSortieEncasDeProlongation|date(\"m/d/Y\") }}</td>
                                <td class=\"cell100 column10\">{{ patient.NombreDejoursResterDansConfinement }}</td>
                                <td class=\"cell100 column11\">{% if patient.TestCovid ==\"TestCovid\" %}
                                    Non rensigne
                                {% else %}
                                      {{patient.TestCovid}}  
                                {% endif %}</td>
                            
                            </tr>
                        {% else %}
                            <tr>
                                <td class=\"cell100 column1\">no records found</td>
                            </tr>
                        {% endfor %}

\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t</table>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div> 

{% endblock %}
", "index.html.twig", "C:\\wamp64\\www\\covid-19\\templates\\index.html.twig");
    }
}
