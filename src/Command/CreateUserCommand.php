<?php 


namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Controller\SecurityController;
use Symfony\Component\HttpFoundation\Request;


class CreateUserCommand extends Command
{
    

    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:create-user';

    private $SecurityController;
    private $Request;
    private $UserPasswordEncoderInterface;

    public function __construct(SecurityController $SecurityController,Request $Request)
    {
        // best practices recommend to call the parent constructor first and
        // then set your own properties. That wouldn't work in this case
        // because configure() needs the properties set in this constructor
        $this->SecurityController = $SecurityController;
        $this->SecurityController = $SecurityController;
        

        parent::__construct();
    }

    

    protected function configure()
    {
        $this
        ->setDescription('Creates a new user.')

        // the full command description shown when running the command with
        // the "--help" option
        ->setHelp('This command allows you to create a user...')
    ;
    }

  

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $message=$this->SecurityController->creationUser();

        printf($message);
      
    }
}