<?php

namespace App\Entity;

use App\Repository\PatientCovidRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PatientCovidRepository::class)
 */
class PatientCovid
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Prenom;

    /**
     * @ORM\Column(type="date")
     */
    private $DateDeNaissance;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Adresse;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=0)
     */
    private $Telephone;

    /**
     * @ORM\Column(type="date")
     */
    private $DateEntrer;

    /**
     * @ORM\Column(type="date")
     */
    private $DateSortie;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $DateSortieEncasDeProlongation;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=0)
     */
    private $NombreDejoursResterDansConfinement;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $TestCovid;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->Nom;
    }

    public function setNom(string $Nom): self
    {
        $this->Nom = $Nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->Prenom;
    }

    public function setPrenom(string $Prenom): self
    {
        $this->Prenom = $Prenom;

        return $this;
    }

    public function getDateDeNaissance(): ?\DateTimeInterface
    {
        return $this->DateDeNaissance;
    }

    public function setDateDeNaissance(\DateTimeInterface $DateDeNaissance): self
    {
        $this->DateDeNaissance = $DateDeNaissance;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->Adresse;
    }

    public function setAdresse(string $Adresse): self
    {
        $this->Adresse = $Adresse;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->Telephone;
    }

    public function setTelephone(string $Telephone): self
    {
        $this->Telephone = $Telephone;

        return $this;
    }

    public function getDateEntrer(): ?\DateTimeInterface
    {
        return $this->DateEntrer;
    }

    public function setDateEntrer(\DateTimeInterface $DateEntrer): self
    {
        $this->DateEntrer = $DateEntrer;

        return $this;
    }

    public function getDateSortie(): ?\DateTimeInterface
    {
        return $this->DateSortie;
    }

    public function setDateSortie(\DateTimeInterface $DateSortie): self
    {
        $this->DateSortie = $DateSortie;

        return $this;
    }

    public function getDateSortieEncasDeProlongation(): ?\DateTimeInterface
    {
        return $this->DateSortieEncasDeProlongation;
    }

    public function setDateSortieEncasDeProlongation(?\DateTimeInterface $DateSortieEncasDeProlongation): self
    {
        $this->DateSortieEncasDeProlongation = $DateSortieEncasDeProlongation;

        return $this;
    }

    public function getNombreDejoursResterDansConfinement(): ?string
    {
        return $this->NombreDejoursResterDansConfinement;
    }

    public function setNombreDejoursResterDansConfinement(string $NombreDejoursResterDansConfinement): self
    {
        $this->NombreDejoursResterDansConfinement = $NombreDejoursResterDansConfinement;

        return $this;
    }

    public function getTestCovid(): ?string
    {
        return $this->TestCovid;
    }

    public function setTestCovid(?string $TestCovid): self
    {
        $this->TestCovid = $TestCovid;

        return $this;
    }
}
