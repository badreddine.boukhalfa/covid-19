<?php

namespace App\Repository;

use App\Entity\PatientCovid;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PatientCovid|null find($id, $lockMode = null, $lockVersion = null)
 * @method PatientCovid|null findOneBy(array $criteria, array $orderBy = null)
 * @method PatientCovid[]    findAll()
 * @method PatientCovid[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PatientCovidRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PatientCovid::class);
    }

    // /**
    //  * @return PatientCovid[] Returns an array of PatientCovid objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PatientCovid
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
