<?php

namespace App\Service;
use App\Entity\User;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;



class ValideFormilaure
{

    private $entitymanager;
    private $userPasswordEncoderInterface;
    
    public function __construct(EntityManagerInterface $entitymanager,UserPasswordEncoderInterface $userPasswordEncoderInterface)
    {
        // best practices recommend to call the parent constructor first and
        // then set your own properties. That wouldn't work in this case
        // because configure() needs the properties set in this constructor
       
        $this->entitymanager=$entitymanager;
        $this->userPasswordEncoderInterface=$userPasswordEncoderInterface;
      
        
      
    }

    protected function CreationUser($password,$username)
    {
        $passwordEncoded = $this->userPasswordEncoderInterface->encodePassword($this->user, $password);
        $this->user->setPassword($passwordEncoded);
        $this->user->setUserName($username);
        $this->entitymanager->persist($this->user);
        $this->entitymanager->flush();

       


    }

    function message ()
    {

        $message='hello';

        return $message;

    }
}