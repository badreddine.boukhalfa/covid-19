<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Form\UserType;

class SecurityController extends AbstractController
{
    /**
     * @Route("/", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // if ($this->getUser()) {
        //     return $this->redirectToRoute('target_path');
        // }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/inscription", name="app_signin")
     */
    public function inscription (Request $request,UserPasswordEncoderInterface $encoder)
    {
        $User = new User();
        $form = $this->createForm(UserType::class, $User);
		if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) 
		{
            $passwordEncoded = $encoder->encodePassword($User, $User->getPassword()); //encoder password 
            $User->setPassword($passwordEncoded);//modfier le password avec le crypté
            $em = $this->getDoctrine()->getManager();
            $em->persist($User);
            $em->flush();
            $lastUsername= $User->getUsername();
			$this->addFlash('success', 'profile creatd !');
			return $this->redirectToRoute('app_login', ['last_username' => $lastUsername]);
		}
        return $this->render('security/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

    public function creationUser()
    {

        $messsage='hello';

        return $messsage;

    }
}
