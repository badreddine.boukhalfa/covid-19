<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\PatientCovid;
use App\Repository\PatientCovidRepository;
use App\Form\PatientCovidType;
use Symfony\Component\HttpFoundation\Request;

class PatientCovidController extends AbstractController
{
    /**
     * @Route("/admin", name="patient_covid")
     */
    public function index(PatientCovidRepository $patientCovidRepository)
    {
        
        $patients=$patientCovidRepository->findAll();
        return $this->render('index.html.twig', [
            'controller_name' => 'Information  Patient Covid-19',
			'patients'=>$patients,
        ]);
    }
	
	 /**
     * @Route("admin/patient/creat", name="patient_covid_creat")
     */
    public function NewPatientCovid(Request $request)
    {
		$patient=new PatientCovid();
		$form = $this->createForm(PatientCovidType::class, $patient);
        
        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) 
        {
            // do some sort of processing
            $datesortie=$patient->getDateEntrer()->getTimestamp()+693938;
            //dump($patient->getDateEntrer());
            //dump($datesortie);
          
            $joursrestant=($datesortie-$patient->getDateEntrer()->getTimestamp())/86400;
            //dump($joursrestant);
            $date = new \DateTime();
            $date->setTimestamp($datesortie);
            $datetimeFormat = 'y-m-d H:00:00.0';
            $datesortiepartient=$date->format($datetimeFormat);
            //dump($datesortiepartient);
            //die();
            $patient->setDateSortie(\DateTime::createFromFormat('y-m-d H:00:00.0',$datesortiepartient));
            $patient->setNombreDejoursResterDansConfinement($joursrestant);
            $em = $this->getDoctrine()->getManager();
            $em->persist($patient);
            $em->flush();
            $this->addFlash(
                'success',
                'Le patient a été ajouter avec succée !'
            );
           
            return $this->redirectToRoute('patient_covid');
        }
        
        return $this->render('patient_covid/new.html.twig', [
            'controller_name' => 'Information  Patient Covid-19',
			'form'=>$form->createView(),
        ]);
    }

     /**
     * @Route("admin/patient/edit/{id}", name="patient_covid_edit")
     */
    public function EditPatientCovid(Request $request,PatientCovidRepository $patientCovidRepository,$id)
    {


        $patient=$patientCovidRepository->find($id);
        $form = $this->createForm(PatientCovidType::class, $patient);
        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) 
        {
             // do some sort of processing
             $datesortie=$patient->getDateEntrer()->getTimestamp()+693938;
             //dump($patient->getDateEntrer());
             //dump($datesortie);
           
             $joursrestant=($datesortie-$patient->getDateEntrer()->getTimestamp())/86400;
             //dump($joursrestant);
             $date = new \DateTime();
             $date->setTimestamp($datesortie);
             $datetimeFormat = 'y-m-d H:00:00.0';
             $datesortiepartient=$date->format($datetimeFormat);
             //dump($datesortiepartient);
             //die();
             $patient->setDateSortie(\DateTime::createFromFormat('y-m-d H:00:00.0',$datesortiepartient));
             $patient->setNombreDejoursResterDansConfinement($joursrestant);
             $em = $this->getDoctrine()->getManager();
             $em->persist($patient);
             $em->flush();
            $this->addFlash(
                'success',
                'update avec succée  !'
            );
           
            return $this->redirectToRoute('patient_covid');
        }

        return $this->render('patient_covid/edit.html.twig', [
            'controller_name' => 'Information  Patient Covid-19',
            'form'=> $form->createView(),
            'id'=>$id,
        ]);

    }

     /**
     * @Route("admin/patient/delete/{id}", name="patient_covid_delete")
     */
    public function DeletePatientCovid(Request $request,PatientCovidRepository $patientCovidRepository,$id)
    {

        $patients=$patientCovidRepository->findAll();
        $patient=$patientCovidRepository->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($patient);
        $em->flush();

        $patients=$patientCovidRepository->findAll();
        return $this->render('index.html.twig', [
            'controller_name' => 'Information  Patient Covid-19',
			'patients'=>$patients,
        ]);

    }

    
}
